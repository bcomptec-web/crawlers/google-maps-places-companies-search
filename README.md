# google-maps-places-companies-search

Find companies on Google Maps by searching with Google Maps Places API.

## Dependencies
- [google-maps-services-go](https://github.com/googlemaps/google-maps-services-go): `go get googlemaps.github.io/maps`
    - https://github.com/googlemaps/google-maps-services-go/blob/master/places.go
    - https://github.com/googlemaps/google-maps-services-go/blob/master/places_test.go
- [kr/pretty](https://github.com/kr/pretty):`go get github.com/kr/pretty`

### Go Modules
```
GO111MODULE=on go mod init gitlab.com/bcomptec-web/crawlers/google-maps-places-companies-search
GO111MODULE=on go build
GO111MODULE=on go mod tidy
```

## Install
`go get gitlab.com/bcomptec-web/crawlers/google-maps-places-companies-search`

## Run
`go run main.go --api-key=YOUR_API_KEY --input-file=input.json`  

Inside `input.json` you can define keywords and location geo coordinates to search for.  

## [Google Maps Places API](https://developers.google.com/places/web-service/intro)

### [Place search](https://developers.google.com/places/web-service/search)
Return a list of places based on a user’s location or search string. 
- Find Place request
- Nearby Search request
- Text Search request

NOTE:  
- Nearby Search and Text Search return all of the available data fields for the selected place (a subset of the supported fields), and you will be billed accordingly   
- There is no way to constrain Nearby Search or Text Search to only return specific fields.   
- To keep from requesting (and paying for) data that you don't need, use a Find Place request instead.   

#### [Find Place request](https://developers.google.com/places/web-service/search#FindPlaceRequests)

[Place Data fields](https://developers.google.com/places/web-service/place-data-fields)

Example query:
```
https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=software&inputtype=textquery&fields=name,formatted_address,type&locationbias=circle:50000@48.8,9.533333&key=YOUR_API_KEY
```

### [Nearby Search request](https://developers.google.com/places/web-service/search#PlaceSearchRequests)

### [Place details](https://developers.google.com/places/web-service/details)
- Return detailed information about a specific place, including user reviews.