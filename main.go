package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/kr/pretty"
	_ "github.com/mattn/go-sqlite3"
	"googlemaps.github.io/maps"
)

func inSlice(a string, slice []validCandidateType) bool {
	for _, b := range slice {
		if string(b) == a {
			return true
		}
	}
	return false
}

type validCandidateType string

const (
	validCandidateTypePointOfInterest = validCandidateType("point_of_interest")

	validCandidateTypeEstablishment = validCandidateType("establishment")
)

var validCandidateTypes = []validCandidateType{validCandidateTypePointOfInterest, validCandidateTypeEstablishment}

func hasValidCandidateTypes(candidate maps.PlacesSearchResult) bool {
	for _, candidateType := range candidate.Types {
		if inSlice(candidateType, validCandidateTypes) {
			return true
		}
	}
	return false
}

func isValidCandidate(candidate maps.PlacesSearchResult) bool {
	return hasValidCandidateTypes(candidate)
}

func typesString(candidate maps.PlacesSearchResult) string {
	return strings.Join(candidate.Types, ",")
}

// The Company representation
type Company struct {
	ID        string
	Name      string
	Address   string
	Website   string
	Latitude  float64
	Longitude float64
}

// The PlacesSearch API
type PlacesSearch interface {
	FindCompanyByTextSearch(query string) Company
	FindCompaniesByNearbySearch(query string) ([]Company, bool)
}

// CompaniesFinder implements the PlacesSearch API
type CompaniesFinder struct {
	Client  *maps.Client
	Storage *output
	Meta    map[string]interface{}
}

// NewCompaniesFinder creates a new CompaniesFinder object
func NewCompaniesFinder(apiKey string, storage *output) *CompaniesFinder {
	client, err := maps.NewClient(maps.WithAPIKey(apiKey))
	if err != nil {
		log.Printf("fatal error: %s\n", err)
		return nil
	}
	return &CompaniesFinder{Client: client, Storage: storage, Meta: make(map[string]interface{})}
}

// FindCompanyByTextSearch searches for a single place that matches a text query
func (finder *CompaniesFinder) FindCompanyByTextSearch(query string) (*Company, bool) {
	findPlaceFromTextRequest := &maps.FindPlaceFromTextRequest{
		Input:              query,
		InputType:          maps.FindPlaceFromTextInputTypeTextQuery,
		Fields:             []maps.PlaceSearchFieldMask{maps.PlaceSearchFieldMaskName, maps.PlaceSearchFieldMaskFormattedAddress, maps.PlaceSearchFieldMaskTypes},
		LocationBias:       maps.FindPlaceFromTextLocationBiasCircular,
		LocationBiasCenter: &maps.LatLng{Lat: 45.6523093, Lng: 25.6102746},
		LocationBiasRadius: 50000,
		// TODO: Add support for specifying Type: establishment, point_of_interest
	}
	findPlaceFromTextResponse, err := finder.Client.FindPlaceFromText(context.Background(), findPlaceFromTextRequest)
	if err != nil {
		log.Printf("find place from text fatal error: %s\n", err)
		return nil, false
	}

	pretty.Logln(findPlaceFromTextResponse)

	numberOfCandidates := len(findPlaceFromTextResponse.Candidates)
	if 1 != numberOfCandidates {
		log.Printf("Expected %+v, was %+v\n", 1, numberOfCandidates)
	} else {
		log.Printf("Number of candidates: %d\n", numberOfCandidates)
	}

	for index, candidate := range findPlaceFromTextResponse.Candidates {
		if isValidCandidate(candidate) {
			log.Printf("[#%d] Name: %s, Address: %s, Types: %s\n", index+1, candidate.Name, candidate.FormattedAddress, typesString(candidate))
			return &Company{Name: candidate.Name, Address: candidate.FormattedAddress}, true
		}
		log.Printf("Invalid candidate: %s, %s \n", candidate.Name, typesString(candidate))
		break
	}
	return nil, false
}

func (finder *CompaniesFinder) doCompaniesByNearbySearch(request *maps.NearbySearchRequest) ([]Company, bool) {
	log.Printf("doCompaniesByNearbySearch - Keyword: %s, Location: (%f, %f), Radius: %d, nextToken: %s",
		request.Keyword,
		request.Location.Lat,
		request.Location.Lng,
		request.Radius, request.PageToken)
	var companies []Company

	nearbySearchResponse, err := finder.Client.NearbySearch(context.Background(), request)
	if err != nil {
		log.Printf("nearby search fatal error: %s\n", err)
		return companies, false
	}

	//pretty.Logln(nearbySearchResponse)

	numberOfResults := len(nearbySearchResponse.Results)
	log.Printf("Number of results: %d\n", numberOfResults)

	for index, result := range nearbySearchResponse.Results {
		if isValidCandidate(result) {
			address := result.Vicinity
			if result.FormattedAddress != "" {
				address = result.FormattedAddress
			}
			log.Printf("[#%d] ID: %s, Name: %s, Address: %s, Types: %s\n", index+1, result.PlaceID, result.Name, address, typesString(result))
			company := Company{
				ID:        result.PlaceID,
				Name:      result.Name,
				Address:   address,
				Latitude:  result.Geometry.Location.Lat,
				Longitude: result.Geometry.Location.Lng,
			}
			companies = append(companies, company)
			if finder.Storage.IncrementalUpsert {
				finder.Storage.upsertWithMetadata(company, request.Keyword, finder.Meta["locationName"].(string))
			}
		}
	}

	if nearbySearchResponse.NextPageToken != "" {
		request.PageToken = nearbySearchResponse.NextPageToken
		log.Printf("Nearby search continues to next page using token: %s", request.PageToken)
		time.Sleep(10 * time.Second)
		if nextPageCompanies, ok := finder.doCompaniesByNearbySearch(request); ok {
			companies = append(companies, nextPageCompanies...)
		} else {
			return companies, false
		}
	}

	return companies, true
}

// FindCompaniesByNearbySearch searches for companies nearby a certain location
func (finder *CompaniesFinder) FindCompaniesByNearbySearch(location maps.LatLng, radius uint, query string) (companies []Company, ok bool) {
	request := &maps.NearbySearchRequest{
		Location: &location,
		Radius:   radius,
		Keyword:  query,
		Language: "en",
	}

	companies, ok = finder.doCompaniesByNearbySearch(request)
	return
}

func runFindCompanyByTextSearch(companiesFinder *CompaniesFinder) {
	if company, ok := companiesFinder.FindCompanyByTextSearch("software"); ok {
		pretty.Println(*company)
	}
}

// GetPlaceDetails returns the details of the place
func (finder *CompaniesFinder) GetPlaceDetails(placeID string) (*maps.PlaceDetailsResult, bool) {
	placeDetailsRequest := &maps.PlaceDetailsRequest{
		PlaceID:  placeID,
		Language: "en",
		Fields:   []maps.PlaceDetailsFieldMask{maps.PlaceDetailsFieldMaskWebsite},
	}
	placeDetailsResponse, err := finder.Client.PlaceDetails(context.Background(), placeDetailsRequest)
	if err != nil {
		log.Printf("PlaceDetails returned non nil error: %v", err)
		return nil, false
	}
	return &placeDetailsResponse, true
}

// GetPlaceWebsite returns the website of the place
func (finder *CompaniesFinder) GetPlaceWebsite(placeID string) string {
	if details, ok := finder.GetPlaceDetails(placeID); ok {
		return details.Website
	}
	return ""
}

type location struct {
	Name string `json:"name"`
	maps.LatLng
}

type input struct {
	Keywords  []string   `json:"keywords"`
	Locations []location `json:"locations"`
}

func getInput(inputFile string) input {
	var jsonObj input
	jsonBin, err := ioutil.ReadFile(inputFile)
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	json.Unmarshal(jsonBin, &jsonObj)
	return jsonObj
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type output struct {
	Database          *sql.DB
	IncrementalUpsert bool
}

func (o *output) createSchema() {
	sqlStmt := `
        CREATE TABLE IF NOT EXISTS companies(
            id text not null primary key,
            name text not null,
            address text not null,
            latitude float not null,
            longitude float not null,
            keyword text,
			location text,
			website text,
            itime timestamp not null default CURRENT_TIMESTAMP,
            utime timestamp not null default CURRENT_TIMESTAMP
        );

        CREATE INDEX IF NOT EXISTS companies_name_index ON companies(name);
    `

	tx, err := o.Database.Begin()
	checkError(err)

	_, err = o.Database.Exec(sqlStmt)
	checkError(err)

	tx.Commit()
}

func (o *output) hasCompany(c Company) bool {
	var exists bool
	stmt, err := o.Database.Prepare("select exists(select 1 from companies where id = ?)")
	checkError(err)
	defer stmt.Close()

	row := stmt.QueryRow(c.ID)
	switch err := row.Scan(&exists); err {
	case nil:
		return exists
	default:
		if err != sql.ErrNoRows {
			log.Print(err)
		}

		return false
	}
}

func (o *output) insert(c Company) bool {
	stmt, err := o.Database.Prepare("INSERT INTO companies(id, name, address, latitude, longitude) values(?,?,?,?,?)")
	checkError(err)
	defer stmt.Close()

	result, err := stmt.Exec(c.ID, c.Name, c.Address, c.Latitude, c.Longitude)
	checkError(err)
	if rows, err := result.RowsAffected(); err == nil && rows > 1 {
		return true
	}
	return false
}

func (o *output) insertWithMetadata(c Company, keyword, place string) bool {
	stmt, err := o.Database.Prepare("INSERT INTO companies(id, name, address, latitude, longitude, keyword, location) values(?,?,?,?,?,?,?)")
	checkError(err)
	defer stmt.Close()

	result, err := stmt.Exec(c.ID, c.Name, c.Address, c.Latitude, c.Longitude, keyword, place)
	checkError(err)
	if rows, err := result.RowsAffected(); err == nil && rows > 1 {
		return true
	}
	return false
}

func (o *output) update(c Company) bool {
	stmt, err := o.Database.Prepare("UPDATE companies SET name = ?, address = ?, latitude = ?, longitude = ?, utime = CURRENT_TIMESTAMP WHERE id = ?")
	checkError(err)
	defer stmt.Close()

	result, err := stmt.Exec(c.Name, c.Address, c.Latitude, c.Longitude, c.ID)
	checkError(err)
	if rows, err := result.RowsAffected(); err == nil && rows > 1 {
		return true
	}
	return false
}

func (o *output) upsert(c Company) {
	if o.hasCompany(c) {
		log.Printf("Company: %s, %s already exists\n", c.ID, c.Name)
		o.update(c)
	} else {
		o.insert(c)
	}
}

func (o *output) upsertWithMetadata(c Company, keyword, place string) {
	if o.hasCompany(c) {
		log.Printf("[upsertWithMetadata][UPDATE] %s - %s", c.ID, c.Name)
		o.update(c)
	} else {
		log.Printf("[upsertWithMetadata][INSERT] %s - %s", c.ID, c.Name)
		o.insertWithMetadata(c, keyword, place)
	}
}

func (o *output) saveAll(keyword, location string, companies []Company) {
	for _, company := range companies {
		o.upsertWithMetadata(company, keyword, location)
	}
}

func (o *output) getPlaceIds(locations []string) (placeIDs []string) {
	numberOfLocations := len(locations)
	if numberOfLocations == 0 {
		return
	}

	args := make([]interface{}, numberOfLocations)
	for i, location := range locations {
		args[i] = location
	}
	rows, err := o.Database.Query(`SELECT id FROM companies where location in (?`+strings.Repeat(",?", len(args)-1)+`)`, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	for rows.Next() {
		var placeID string
		if err := rows.Scan(&placeID); err != nil {
			log.Fatal(err)
		}
		placeIDs = append(placeIDs, placeID)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	return
}

func newOutput(incrementalUpsert bool) *output {
	db, err := sql.Open("sqlite3", "./output.db")
	checkError(err)

	obj := &output{Database: db, IncrementalUpsert: incrementalUpsert}
	obj.createSchema()
	return obj
}

func (o *output) Cleanup() {
	defer o.Database.Close()
}

func (o *output) UpdatePlaceWithWebsite(placeID, placeWebsite string) bool {
	stmt, err := o.Database.Prepare("UPDATE companies SET website = ?, utime = CURRENT_TIMESTAMP WHERE id = ? AND website IS NULL")
	checkError(err)
	defer stmt.Close()

	result, err := stmt.Exec(placeWebsite, placeID)
	checkError(err)
	if rows, err := result.RowsAffected(); err == nil && rows > 1 {
		return true
	}
	return false
}

// FindCompanies finds companies with Google Maps Places Nearby Search API
func FindCompanies(companiesFinder *CompaniesFinder, inputData input, outputData output) {
	for _, location := range inputData.Locations {
		log.Printf("Searching for companies in location: %s\n", location.Name)
		companiesFinder.Meta["locationName"] = location.Name
		for _, keyword := range inputData.Keywords {
			var radius uint
			for radius = 10000; radius <= 50000; radius += 10000 {
				if companies, ok := companiesFinder.FindCompaniesByNearbySearch(
					location.LatLng,
					radius,
					keyword,
				); ok {
					log.Printf("Found #%d companies for location: %s, keyword: %s, radius: %d\n", len(companies), location.Name, keyword, radius)
					if !outputData.IncrementalUpsert {
						outputData.saveAll(keyword, location.Name, companies)
					}
				} else {
					log.Printf("Didn't find any companies for location: %s, keyword: %s -- Check above logs!\n", location.Name, keyword)
					os.Exit(1)
				}
			}
		}
	}
}

// UpdatePlacesWithWebsite adds website field to places
func UpdatePlacesWithWebsite(companiesFinder *CompaniesFinder, outputData *output, placeIDs []string) {
	for _, placeID := range placeIDs {
		placeWebsite := companiesFinder.GetPlaceWebsite(placeID)
		time.Sleep(2 * time.Second)
		if len(placeWebsite) != 0 {
			log.Printf("Adding website %s for placeID: %s\n", placeWebsite, placeID)
			outputData.UpdatePlaceWithWebsite(placeID, placeWebsite)
		}
	}
}

func main() {
	apiKey := flag.String("api-key", "", "Google Maps Places API key")
	inputFilePath := flag.String("input-file", "", "The input file path")
	flag.Parse()

	if apiKey == nil || *apiKey == "" {
		log.Fatalln("Please provide a valid API key")
	}

	if inputFilePath == nil || *inputFilePath == "" {
		log.Fatalln("Please provide a valid input file")
	}

	//inputData := getInput(*inputFilePath)
	outputData := newOutput(true)
	defer outputData.Cleanup()
	companiesFinder := NewCompaniesFinder(*apiKey, outputData)

	// Search for companies
	// FindCompanies(companiesFinder, inputData, outputData)

	// Add website field to companies
	placeIDs := outputData.getPlaceIds([]string{"Stuttgart", "Schorndorf", "Miedelsbach"})
	UpdatePlacesWithWebsite(companiesFinder, outputData, placeIDs)
}
