select location, keyword, count(*)
from companies
group by 1, 2
order by 1, 3 desc, 2

select id, name, name as partner_name, address as street, "Germany" as country_id, 'Google Maps Places' as tag_ids, 'Emanuel' as user_id
from companies
where location in ('Miedelsbach', 'Schorndorf', 'Stuttgart')
order by keyword desc

-- Companies with website
SELECT count(distinct name)
from companies 
where location in ('Miedelsbach', 'Schorndorf', 'Stuttgart') and website is not null

-- Companies with no website
select count(distinct name)
from companies
where location in ('Miedelsbach', 'Schorndorf', 'Stuttgart') and website is null

select distinct(name)
from companies
where location in ('Miedelsbach', 'Schorndorf', 'Stuttgart') and website is null
order by 1

-- Export companies with website
select id, name, name as partner_name, address as street, "Germany" as country_id, 'Google Maps Places' as tag_ids, 'Emanuel' as user_id, website
from companies
where location in ('Miedelsbach', 'Schorndorf', 'Stuttgart') and website is not null
order by keyword desc