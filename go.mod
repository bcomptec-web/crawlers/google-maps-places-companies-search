module gitlab.com/bcomptec-web/crawlers/google-maps-places-companies-search

go 1.12

require (
	github.com/google/uuid v1.1.1 // indirect
	github.com/kr/pretty v0.1.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/stretchr/testify v1.4.0 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	googlemaps.github.io/maps v0.0.0-20190731233030-3b2ef8dcfc73
)
